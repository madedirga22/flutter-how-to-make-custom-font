import 'package:flutter/material.dart';

Color primaryColor = Colors.black;

TextStyle primaryTextStyle = TextStyle(
  fontFamily: "Poppins",
  color: primaryColor,
);
